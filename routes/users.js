var express = require('express');
var router = express.Router();
var db = require('../db')
var sessionMap = new Map();
var minesweeperMap = new Map();

router.post('/login', function(req, res) {
    //compare hashedpassword (ajax always pass hashed password)
    var username = req.body.userid;
    var hashedpassword = req.body.hashedPassword;
    hashedpassword = JSON.stringify(hashedpassword);
    //only one row (Shouldn't have duplicate id)
    db.one('SELECT * FROM account where username = ${username}', {username: username})
    .then(function(data){
        var serverPassword = data.password;
        if(String(hashedpassword).toUpperCase() === String(serverPassword).toUpperCase()){
            //Correct password --> login succeed
            var sessionid = Math.floor(Math.random()*1234567890+1);
            while(sessionMap.has(sessionid)){
                sessionid = Math.floor(Math.random()*1234567890+1);
            }
            sessionMap.set(sessionid, username);
            
            res.status(200)
            .json({
                status: 'success',
                message: 'Login succeed!',
                sessionid: sessionid
            });
        } else {
            res.status(200)
            .json({
                status: 'error',
                message: 'Password wrong.'
            })
        }
    })
    .catch(function(error){
        res.status(200)
        .json({
            status: 'error',
            message: 'Account not exist!'
        })
    });

});

router.post('/register', function(req, res){
    //add record to account table
    var username = req.body.userid;
    var email = req.body.email;
    var hashedpassword = req.body.hashedpassword;
    db.any('SELECT * FROM account where username = $1', username).then(function(data) {
        //console.log("db access");
        if(!(typeof data == 'undefined' || data.length == 0)) {
            //username already exist
            res.status(200).json({
                status: 'error',
                message: data + `ID '${username}' already exist!`
            });
        } else {
            db.none('INSERT INTO account(username, email, password) VALUES (${username}, ${email}, ${password})', {
                username: username,
                email: email,
                password: hashedpassword
            });
            db.none('INSERT INTO minesweeper(username) values (${username})', {username: username});
            res.status(200)
            .json({
                status: 'success',
                message: 'Register complete'
            })
        }
    });
});

router.post('/profile', function(req, res){
    //show user details in minesweeper table
    var sessionid = Number(req.body.sessionid);
    if(!sessionMap.has(sessionid)){
        res.status(200)
        .json({
            status: "error",
            message: "Please login again." + sessionid + " " + sessionMap.get(sessionid)
        });
        return;
    }
    var username = sessionMap.get(sessionid);
    //unique username -> only one row return
    db.one('SELECT * FROM account ac INNER JOIN minesweeper score ON ac.username = score.username where ac.username = ${username}', {username: username})
    .then(function(data){
        //console.log(data);
        var username = data.username;
        var email = data.email;
        var minesweeper = {
            easy_time: data.easy_time/1000,
            medium_time: data.medium_time/1000,
            hard_time: data.hard_time/1000,
            custom_size: data.custom_size,
            custom_time: data.custom_time/1000
        };
        res.status(200)
        .json({
            status: "success",
            username: username,
            email: email,
            minesweeper: minesweeper,
        });
    });
});

router.post('/leaderboard', function(req, res){
    //select from minesweeper_leaderboard // insert or alter leaderboard
    var type = req.body.type;
    var difficulty = req.body.difficulty;
    if(type === "pull"){
        db.any('SELECT * FROM minesweeper_leaderboard WHERE type = ${difficulty} ORDER BY rank', {difficulty: difficulty})
        .then(function(data){
            res.status(200)
            .json({
                status: "success",
                message: data, 
            });
        });
    }
});

router.post('/logout', function(req, res){
    //handle logout event
    var sessionid = req.body.sessionid;
    sessionMap.delete(sessionid);
    res.status(200)
    .json({
        status: "success",
    })
});

function randomMinesweeperMap(height, width, bombNumber){
    var bombPosition = [];
    var minesweeperArray = [];
    for(var i = 0; i < bombNumber; i++){
        var newPos = Math.floor(Math.random()*height*width+1);
        while(bombPosition.length != 0 && bombPosition.includes(newPos)){
            newPos = Math.floor(Math.random()*height*width+1);
        }
        bombPosition.push(newPos);
    }
    bombPosition.sort(function(a, b){ return a-b});
    //create all 0 array
    for(var i = 0; i < height; i++){
        var data = [];
        for(var j = 0; j < width; j++){
            data.push(0);
        }
        minesweeperArray.push(data);
    }
    bombPosition.forEach(element => {
        var i = Math.floor(element/width);
        var j = element%width;
        minesweeperArray[i][j] = "*";
    });

    for(var i = 0; i < height; i++){
        for(var j = 0; j < width; j++){
            if(minesweeperArray[i][j] == "*"){
                if(i-1 >= 0 && j-1 >= 0){
                    if(minesweeperArray[i-1][j-1] != "*"){
                        minesweeperArray[i-1][j-1]++;
                    }
                }
                if(i-1 >= 0 && j+1 < width){
                    if(minesweeperArray[i-1][j+1] != "*"){
                        minesweeperArray[i-1][j+1]++;
                    }
                }
                if(i-1 >= 0){
                    if(minesweeperArray[i-1][j] != "*"){
                        minesweeperArray[i-1][j]++;
                    }
                }
                if(i+1 < height && j-1 >= 0){
                    if(minesweeperArray[i+1][j-1] != "*"){
                        minesweeperArray[i+1][j-1]++;
                    }
                }
                if(i+1 < height && j+1 < width){
                    if(minesweeperArray[i+1][j+1] != "*"){
                        minesweeperArray[i+1][j+1]++;
                    }
                }
                if(i+1 < height){
                    if(minesweeperArray[i+1][j] != "*"){
                        minesweeperArray[i+1][j]++;
                    }
                }
                if(j-1 >= 0){
                    if(minesweeperArray[i][j-1] != "*"){
                        minesweeperArray[i][j-1]++;
                    }
                }
                if(j+1 < width){
                    if(minesweeperArray[i][j+1] != "*"){
                        minesweeperArray[i][j+1]++;
                    }
                }
            }
        }
    }
    return minesweeperArray;
}

function chainOpen(obj, i, j, height, width){
    if(!(i < 0 || i >= height || j < 0 || j >= width) && (obj.opened[i][j] == 0 || obj.opened[i][j] == null)){
        //obj pass by reference
        obj.opened[i][j] = 1;
        var bombNearby = false;
        if(i-1 >= 0){
            if(j-1 >= 0){
                if(obj.answer[i-1][j-1] == "*"){
                    bombNearby = true;
                }
            }
            if(obj.answer[i-1][j] == "*"){
                bombNearby = true;
            }
            if(j+1 < width){
                if(obj.answer[i-1][j+1] == "*"){
                    bombNearby = true;
                }
            }
        }
        if(i+1 < height){
            if(j-1 >= 0){
                if(obj.answer[i+1][j-1] == "*"){
                    bombNearby = true;
                }
            }
            if(obj.answer[i+1][j] == "*"){
                bombNearby = true;
            }
            if(j+1 < width){
                if(obj.answer[i+1][j+1] == "*"){
                    bombNearby = true;
                }
            }
        }
        if(j-1 >= 0){
            if(obj.answer[i][j-1] == "*"){
                bombNearby = true;
            }
        }
        if(j+1 < width){
            if(obj.answer[i][j+1] == "*"){
                bombNearby = true;
            }
        }
        
        if(!bombNearby){
            chainOpen(obj,i-1,j-1, height, width);
            chainOpen(obj,i-1,j, height, width);
            chainOpen(obj,i-1,j+1, height, width);
            chainOpen(obj,i,j-1, height, width);
            chainOpen(obj,i,j+1, height, width);
            chainOpen(obj,i+1,j-1, height, width);
            chainOpen(obj,i+1,j, height, width);
            chainOpen(obj,i+1,j+1, height, width);
        }
    }
}

router.post('/create', function(req, res){
    var sessionid = Number(req.body.sessionid);
    var username = sessionMap.get(sessionid);
    var difficulty = req.body.difficulty;
    var height = req.body.height;
    var width = req.body.width;
    var _delete = req.body.delete;
    console.log(difficulty + " " + height + " " + width);
    if(difficulty == "easy"){
        height = 9;
        width = 9;
    } else if(difficulty == "medium"){
        height = 16;
        width = 16;
    } else if(difficulty == "hard"){
        height = 16;
        width = 30;
    }
    if(_delete){
        minesweeperMap.delete(sessionid);
        res.status(200)
        .json({
            status: 'success',
            message: "done",
        });
        return;
    }
    if(minesweeperMap.get(sessionid)){
        //console.log(minesweeperMap.get(sessionid));
        //already exist -> ask continue or not
        if(minesweeperMap.get(sessionid).username === username){
            var answer = minesweeperMap.get(sessionid).minesweeperMap;
            var userInput = minesweeperMap.get(sessionid).userInput;
            var result = [];
            var height = minesweeperMap.get(sessionid).height;
            var width = minesweeperMap.get(sessionid).width;
            var flagCount = 0;
            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    temp.push(0);
                }
                result.push(temp);
            }

            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    if(userInput[x][y] === 1){
                        result[x][y] = answer[x][y];
                    }
                    if(userInput[x][y] === 1 && answer[x][y] == 0){
                        result[x][y] = "_";
                    }
                    if(userInput[x][y] == "F"){
                        result[x][y] = "F";
                        flagCount++;
                    }
                }
            }
            res.status(200)
            .json({
                status: "success",
                message: minesweeperMap.get(sessionid).difficulty + " Record found, continue?",
                minesweeper: result,
                difficulty: minesweeperMap.get(sessionid).difficulty,
                bombNumber: minesweeperMap.get(sessionid).bombNumber - flagCount,
                time: minesweeperMap.get(sessionid).time,
            });
            return;
        }
    }
    minesweeperMap.forEach((value, key)=>{
        if(value.username == username){
            //different session id but same username (e.g. different machine/browser)
            var map = minesweeperMap.get(key);
            map.sessionid = sessionid;
            minesweeperMap.set(sessionid, map);
            minesweeperMap.delete(key);
            var answer = minesweeperMap.get(sessionid).minesweeperMap;
            var userInput = minesweeperMap.get(sessionid).userInput;
            var result = [];
            var height = minesweeperMap.get(sessionid).height;
            var width = minesweeperMap.get(sessionid).width;
            var flagCount = 0;
            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    temp.push(0);
                }
                result.push(temp);
            }

            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    if(userInput[x][y] === 1){
                        result[x][y] = answer[x][y];
                    }
                    if(userInput[x][y] === 1 && answer[x][y] == 0){
                        result[x][y] = "_";
                    }
                    if(userInput[x][y] == "F"){
                        result[x][y] = "F";
                        flagCount++;
                    }
                }
            }
            res.status(200)
            .json({
                status: "success",
                message: minesweeperMap.get(sessionid).difficulty + " Record found, continue?",
                minesweeper: result,
                difficulty: minesweeperMap.get(sessionid).difficulty,
                bombNumber: minesweeperMap.get(sessionid).bombNumber - flagCount,
                time: minesweeperMap.get(sessionid).time,
            });
            return;
        }
    });
    var userInput = [];
    for(var i = 0; i < height; i++){
        var data = [];
        for(var j = 0; j < width; j++){
            data.push(0);
        }
        userInput.push(data);
    }

    var bombNumber = Math.floor(Math.floor(Math.random(6)+15)*height*width/100); //15-20% bomb
    var map = randomMinesweeperMap(height, width, bombNumber);
    var minesweeper_map = {
        sessionid: sessionid,
        username: username,
        difficulty: difficulty,
        bombNumber: bombNumber,
        height: height,
        width: width,
        minesweeperMap: map,
        userInput: userInput,
        time: 0,
        win: false
    }
    minesweeperMap.set(sessionid, minesweeper_map);
    res.status(200)
    .json({
        status: "success",
        message: "create done",
        bombNumber: bombNumber,
    });
    //console.log("CREATED");
});

function checkWin(userInput, minesweeper_map){
    var bombCount = minesweeper_map.bombNumber;
    var height = minesweeper_map.height;
    var width = minesweeper_map.width;
    var map = minesweeper_map.minesweeperMap;
    
    for(var i = 0; i < height; i++){
        for(var j = 0; j < width; j++){
            if(map[i][j] == "*" && userInput[i][j] == "F"){
                bombCount--;
            }
        }
    }
    if(bombCount == 0){
        //Flag all bomb
        return true;
    }
    var win = true;
    for(var i = 0; i < height; i++){
        for(var j = 0; j < width; j++){
            if(map[i][j] != "*"){
                if(userInput[i][j] != 1){
                    win = false;
                }
            }
        }
    }
    return win;
}

function updateRecord(username, difficulty, custom_h, custom_w, time){
    //user profile
    db.one('SELECT * FROM minesweeper where username = ${username}', {username: username})
    .then(function(data){
        //console.log(data);
        if(difficulty == "custom"){
            var size = custom_h + "x" + custom_w;
            db.none('UPDATE minesweeper SET custom_size = ${custom_size}, custom_time = ${time} WHERE username = ${username}', {custom_size: size, time: time, username: username});
        } else {
            var record;
            var type;
            if(difficulty == "easy"){
                record = data.easy_time;
                type = "easy_time";
            } else if(difficulty == "medium"){
                record = data.medium_time;
                type = "medium_time";
            } else if(difficulty == "hard"){
                record = data.hard_time;
                type = "hard_time";
            }
            record = Number(record);
            if(Number(time) < record || typeof record == 'undefined'){
                //less time or new record
                db.none('UPDATE minesweeper SET ${type:name} = ${time} WHERE username = ${username}', {type: type, time: time, username: username});
            }
        }
    });
    //leaderboard
    db.any('SELECT * FROM minesweeper_leaderboard where type = ${difficulty} ORDER BY rank', {difficulty: difficulty})
    .then(function(data){
        if(data.length == 0){
            //new leaderboard
            db.none('INSERT INTO minesweeper_leaderboard(username, type, rank, time) VALUES (${username}, ${difficulty}, ${rank}, ${time})', {
                username: username,
                difficulty: difficulty,
                rank: 1,
                time: time,
            });
        } else if(data.length < 10){
            //leaderboard with 1-9 record --> sort & compare time
            for(var i = 1; i <= data.length; i++){
                if(time < data[i-1].time){
                    db.none('UPDATE minesweeper_leaderboard SET rank = rank+1 WHERE rank >= ${rank} AND type = ${difficulty}', {
                        rank: i,
                        difficulty: difficulty,
                    });
                    db.none('INSERT INTO minesweeper_leaderboard(username, type, rank, time) VALUES (${username}, ${difficulty}, ${rank}, ${time})',{
                        username: username,
                        difficulty: difficulty,
                        rank: i,
                        time: time,
                    });
                    break;
                }
                if(i == data.length){
                    db.none('INSERT INTO minesweeper_leaderboard(username, type, rank, time) VALUES (${username}, ${difficulty}, ${rank}, ${time})',{
                        username: username,
                        difficulty: difficulty,
                        rank: i+1,
                        time: time,
                    });
                    break;
                }
            }
        } else {
            //full leaderboard -> find -> del rank 10 & push all 
            for(var i = 1; i <= data.length; i++){
                if(time < data[i-1].time){
                    db.none('DELETE FROM minesweeper_leaderboard WHERE rank = 10 AND type = ${difficulty}',{
                        difficulty: difficulty,
                    });
                    db.none('UPDATE minesweeper_leaderboard SET rank = rank+1 WHERE rank >= ${rank} AND type = ${difficulty}', {
                        rank: i,
                        difficulty: difficulty,
                    });
                    db.none('INSERT INTO minesweeper_leaderboard(username, type, rank, time) VALUES (${username}, ${difficulty}, ${rank}, ${time})',{
                        username: username,
                        difficulty: difficulty,
                        rank: i,
                        time: time,
                    });
                    break;
                }
            }
        }
    });
}

router.post('/click', function(req, res){
    var sessionid = Number(req.body.sessionid);
    var username = sessionMap.get(sessionid);
    var minesweeper = minesweeperMap.get(sessionid);
    var i = req.body.i;
    var j = req.body.j;
    var time = req.body.time;
    var clicktype = req.body.click;
    var height = minesweeper.height;
    var width = minesweeper.width;
    var difficulty = minesweeper.difficulty;
    var win = req.body.win;
    if(username != minesweeper.username){
        res.status(200)
        .json({
            status: "error",
            message: "Username doesn't match",
        });
        return;
    }
    if(i < 0 || i > height || j < 0 || j > width){
        res.status(200)
        .json({
            status: "error",
            message: "input location error, please click again.",
        });
        return;
    }
    if(win){
        if(!minesweeper.win){
            //fake win(may triggered by user)
            res.status(200)
            .json({
                status: "error",
                message: "This game haven't win!",
            })
        } else {
            var update = req.body.update;
            if(update){
                updateRecord(username, difficulty, height, width, minesweeper.time);
                minesweeperMap.delete(sessionid);
                res.status(200)
                .json({
                    status: "success",
                    message: "Update done",
                });
            } else {
                minesweeperMap.delete(sessionid);
                res.status(200)
                .json({
                    status: "success",
                    message: "done",
                });
            }
        }
        return;
    }
    var userInput = minesweeper.userInput;
    var minesweeper_map = minesweeper.minesweeperMap;
    console.log(minesweeper_map);
    if(time < minesweeper.time){
        res.status(200)
        .json({
            status: "error",
            message: "time error",
        });
        return;
    }
    minesweeper.time = time;
    if(typeof i == 'undefined' || typeof j == 'undefined'){
        //update time only
        if(time < minesweeper.time){
            res.status(200)
            .json({
                status: "error",
                message: "time error",
            });
            return;
        }
        minesweeper.time = time;
        return;
    }
    if(clicktype == "right"){
        //console.log("RIGHT CLICK!");
        if(userInput[i][j] == "F"){
            //flag exist --> remove F
            userInput[i][j] = 0;
            var flagCount = 0;
            var result = [];
            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    temp.push(0);
                }
                result.push(temp);
            }
            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    if(userInput[x][y] === 1){
                        result[x][y] = minesweeper_map[x][y];
                    }
                    if(userInput[x][y] === 1 && minesweeper_map[x][y] == 0){
                        result[x][y] = "_";
                    }
                    if(userInput[x][y] == "F"){
                        result[x][y] = "F";
                        flagCount++;
                    }
                }
            }
            res.status(200)
            .json({
                status: "success",
                message: "unFlagged",
                result: result,
                bombNumber: minesweeper.bombNumber - flagCount,
            });
            return;
        } else if(userInput[i][j] == 1) {
            res.status(200)
            .json({
                status: "error",
                message: "already clicked",
            });
            return;
        } else {
            userInput[i][j] = "F";
            var flagCount = 0;
            var result = [];
            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    temp.push(0);
                }
                result.push(temp);
            }
            for(var x = 0; x < height; x++){
                var temp = [];
                for(var y = 0; y < width; y++){
                    if(userInput[x][y] === 1){
                        result[x][y] = minesweeper_map[x][y];
                    }
                    if(userInput[x][y] === 1 && minesweeper_map[x][y] == 0){
                        result[x][y] = "_";
                    }
                    if(userInput[x][y] == "F"){
                        result[x][y] = "F";
                        flagCount++;
                    }
                }
            }
            var winOrNot = checkWin(userInput, minesweeper);
            if(winOrNot){
                if(difficulty == "custom"){
                    db.one("SELECT custom_size, custom_time from minesweeper where username = ${username}",{username: username})
                    .then(function(data){
                        console.log(data);
                        res.status(200)
                        .json({
                            status: "success",
                            message: "win",
                            result: result,
                            type: "custom",
                            custom_size: data.custom_size,
                            custom_time: data.custom_time,
                        });
                        minesweeper.win = true;
                        return;
                    });
                    return;
                } else {
                    updateRecord(username, difficulty, height, width, time);
                    res.status(200)
                    .json({
                        status: "success",
                        message: "win",
                        type: "normal",
                        result: result,
                    });
                    minesweeperMap.delete(sessionid);
                    return;
                }
            }
            res.status(200)
            .json({
                status: "success",
                message: "Flagged",
                result: result,
                bombNumber: minesweeper.bombNumber - flagCount,
            });
            return;
        }
    }
    //only left click able to pass above function
    if(minesweeper_map[i][j] == "*"){
        //boooooooom!
        var tempMap = [];
        for(var x = 0; x < height; x++){
            var temp = [];
            for(var y = 0; y < width; y++){
                temp.push(0);
            }
            tempMap.push(temp);
        }
        for(var x = 0; x < height; x++){
            for(var y = 0; y < width; y++){
                if(userInput[x][y] === 1){
                    tempMap[x][y] = minesweeper_map[x][y];
                }
                if(userInput[x][y] === 1 && minesweeper_map[x][y] == 0){
                    tempMap[x][y] = "_";
                }
                if(userInput[x][y] == "F"){
                    tempMap[x][y] = "F";
                }
                if(minesweeper_map[x][y] == "*"){
                    tempMap[x][y] = minesweeper_map[x][y];
                }
            }
        }
        minesweeperMap.delete(sessionid);
        res.status(200)
        .json({
            status: "success",
            message: "BOOM!",
            result: tempMap,
        });
        return;
    } else {
        //number
        var tmp = {
            opened: userInput,
            answer: minesweeper_map,
        }
        chainOpen(tmp, Number(i), Number(j), height, width);
        userInput = tmp.opened;
        var result = [];
        var flagCount = 0;
        for(var x = 0; x < height; x++){
            var temp = [];
            for(var y = 0; y < width; y++){
                temp.push(0);
            }
            result.push(temp);
        }
        for(var x = 0; x < height; x++){
            for(var y = 0; y < width; y++){
                if(userInput[x][y] === 1){
                    result[x][y] = minesweeper_map[x][y];
                }
                if(userInput[x][y] === 1 && minesweeper_map[x][y] == 0){
                    result[x][y] = "_";
                }
                if(userInput[x][y] == "F"){
                    result[x][y] = "F";
                    flagCount++;
                }
            }
        }
        var winOrNot = checkWin(userInput, minesweeper);
        if(winOrNot){
            if(difficulty == "custom"){
                db.one("SELECT custom_size, custom_time from minesweeper where username = ${username}",{username: username})
                .then(function(data){
                    res.status(200)
                    .json({
                        status: "success",
                        message: "win",
                        result: result,
                        type: "custom",
                        custom_size: data.custom_size,
                        custom_time: data.custom_time,
                    });
                    minesweeper.win = true;
                    return;
                });
                return;
            } else {
                updateRecord(username, difficulty, height, width, time);
                res.status(200)
                .json({
                    status: "success",
                    message: "win",
                    type: "normal",
                    result: result,
                });
                minesweeperMap.delete(sessionid);
                return;
            }
        }
        res.status(200)
        .json({
            status: "success",
            message: "Opened",
            result: result,
            bombNumber: minesweeper.bombNumber - flagCount,
        });
    }
});

module.exports = router;
